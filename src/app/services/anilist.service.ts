import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Query } from '../models/query';
import { Variable } from '../models/variable';

@Injectable({
  providedIn: 'root',
})
export class AnilistService {
  private API_URL: string = 'https://graphql.anilist.co';
  private OPTIONS: any = {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  };
  resultsChange: Subject<string> = new Subject<string>();
  media: any;

  constructor(private http: HttpClient, private location: Location) {}

  async changeResults(variables: any) {
    this.media = variables;
    this.resultsChange.next('');
  }

  requestQuery(body: any) {
    return new Promise((resolve, reject) => {
      this.http.post(this.API_URL, body, this.OPTIONS).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  async getMedia(queryVariables: Query[]) {
    const path: string = this.location.path();
    const type: string = path.replace('/browse/', '');

    let variablesMedia: any[] = queryVariables.map(async (query: Query) => {
      const variables: Variable = query.variables;
      variables.perPage = query.label === 'TOP RATED ANIME' ? 10 : 6;
      variables.type = type.toUpperCase();

      const condition: any = {
        query: this.createQuery(),
        variables,
      };

      const body: any = JSON.stringify(condition);
      const response: any = await this.requestQuery(body);

      const variableMedia = {
        label: query.label,
        data: response.data.Page.media,
      };

      return variableMedia;
    });

    variablesMedia = await Promise.all(variablesMedia);
    return variablesMedia;
  }

  async getMediaList(variables: Variable) {
    const condition: any = {
      query: this.createQuery(),
      variables,
    };

    const body: any = JSON.stringify(condition);
    const response: any = await this.requestQuery(body);
    return response.data.Page.media;
  }

  getCategoryCollection() {
    const query = this['genresQuery']();
    const conditions = { query };
    const body = JSON.stringify(conditions);
    return this.requestQuery(body);
  }

  getMediaFullDetails(id: number) {
    const query = this.mediaFullDetailsQuery();
    const variables = { id };
    const conditions = { query, variables };
    const body = JSON.stringify(conditions);
    return this.requestQuery(body);
  }

  mediaFullDetailsQuery() {
    return `
      query ($id: Int) {
        details: Media (id: $id) {
          title {
            romaji english native
          }
          coverImage {
            extraLarge large medium
          }
          bannerImage
          description
          rankings {
            rank type format year season allTime context
          }
          airing: nextAiringEpisode {
            airingAt timeUntilAiring episode
          }
          startDate {
            year month day
          }
          endDate{
            year month day
          }
          studios {
            nodes {
              name
            }
          }
          tags {
            name rank isMediaSpoiler
          }
          externalLinks {
            url site
          }
          relations {
            edges {
              node {
                coverImage {
                   medium
                }
                title {
                  english romaji
                }
                format
                status
              }
              relationType (version: 2)
            }
          }
          characters (sort: ID, role: MAIN) {
            edges {
              id
              role
              node {
                id
                name {
                  full native alternative
                }
                image {
                  large medium
                }
                description siteUrl
              }
              voiceActors {
                name {
                  full native alternative
                }
                language
                image {
                  large medium
                }
                siteUrl
              }
            }
          }
          producers: staff {
            edges {
              node {
                name {
                  full
                }
                image {
                  medium
                }
                siteUrl
              }
              role
            }
          }
          trailer {
            id site thumbnail
          }
          recommendations (sort: RATING_DESC) {
            nodes {
              rating
              mediaRecommendation {
                title {
                  romaji english
                }
                coverImage {
                  large medium color
                }
              }
            }
          }
          streamingEpisodes {
            title thumbnail url site
          }
          format episodes duration status  season seasonYear averageScore 
          meanScore popularity favourites source hashtag genres synonyms
        }
      }
    `;
  }

  genresQuery() {
    return `
      query {
        genres: GenreCollection
        tags: MediaTagCollection { 
          name description category isAdult
        }
      }
    `;
  }

  createQuery() {
    return `
      query ($page: Int, $perPage: Int, $sort: [MediaSort], 
        $season: MediaSeason, $seasonYear: Int, 
        $type: MediaType, $genre: String, $format: 
        MediaFormat, $status: MediaStatus, 
        $source: MediaSource, $search: String) { 
        Page (page: $page, perPage: $perPage) {
          media (type: $type, sort: $sort, season: $season, 
            seasonYear: $seasonYear, search: $search, 
            genre: $genre,  format: $format, status: $status, 
            source: $source) {
            title {
              romaji english
            }
            coverImage {
              extraLarge large medium color
            }
            tags {
              name description category rank
            }
            nextAiringEpisode {
              airingAt timeUntilAiring episode
              media {
                title {
                  romaji english
                }
              }
            }
            studios {
              nodes {
                name
              }
            }
            id genres averageScore popularity episodes format status 
            popularity season seasonYear description type
          }
        }
      }
    `;
  }
}
