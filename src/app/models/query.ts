import { Variable } from '../models/variable';

export interface Query {
  label: string;
  variables: Variable;
}
