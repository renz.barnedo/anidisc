import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Dropdown } from 'src/app/models/dropdown';
import { AnilistService } from 'src/app/services/anilist.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  dropdowns: Dropdown[] = [
    {
      label: 'Genre',
      formControlName: 'genre',
      options: [],
    },
    {
      label: 'Year',
      formControlName: 'seasonYear',
      options: [],
    },
    {
      label: 'Season',
      formControlName: 'season',
      options: ['Winter', 'Spring', 'Summer', 'Fall'],
    },
    {
      label: 'Format',
      formControlName: 'format',
      options: [
        'TV Show',
        'Movie',
        'TV Short',
        'Special',
        'OVA',
        'ONA',
        'Music',
      ],
    },
    {
      label: 'Airing Status',
      formControlName: 'status',
      options: ['Releasing', 'Finished', 'Not Yet Released', 'Cancelled'],
    },
    {
      label: 'Source Material',
      formControlName: 'source',
      options: [
        'Original',
        'Manga',
        'Light Novel',
        'Novel',
        'Anime',
        'Visual Novel',
        'Video Game',
        'Doujinshi',
        'Other',
      ],
    },
  ];
  queryParameters: any = {};
  searchForm: FormGroup = this.fb.group({});
  showForm: boolean = false;

  constructor(
    private fb: FormBuilder,
    private anilistService: AnilistService,
    private router: Router,
    private route: ActivatedRoute,
    private uiService: UiService
  ) {}

  async ngOnInit() {
    try {
      const response: any = await this.anilistService.getCategoryCollection();
      this.dropdowns = this.dropdowns.map((dropdown: any) => {
        if (dropdown.formControlName === 'genre') {
          let { genres, tags } = response.data;
          tags = tags.map((tag: any) => tag.name);
          dropdown.options = ['GENRES label', ...genres, 'TAGS label', ...tags];
        }
        if (dropdown.formControlName === 'seasonYear') {
          dropdown.options = this.getYearValues();
        }
        return dropdown;
      });

      const controls: any = {
        search: this.fb.control('', [
          Validators.maxLength(50),
          Validators.pattern('^[a-zA-Z ]*$'),
        ]),
        genre: this.fb.control(''),
        seasonYear: this.fb.control(''),
        season: this.fb.control(''),
        format: this.fb.control(''),
        status: this.fb.control(''),
        source: this.fb.control(''),
      };

      location.search
        .replace('?', '')
        .split('&')
        .forEach((query: any) => {
          const queryParts = query.split('=');
          const key = queryParts[0];
          let value = queryParts[1];

          if (key && value) {
            this.queryParameters[key] = value;
            if (!isNaN(value)) {
              value = +value;
            }
            if (typeof value === 'string') {
              value = value
                .split('_')
                .map((word: string) => {
                  if (word === 'TV') {
                    return word + ' Show';
                  }
                  if (word === 'OVA' || word === 'ONA') {
                    return word;
                  }
                  return word[0] + word.substring(1).toLowerCase();
                })
                .join(' ');
            }

            controls[key] = this.fb.control(value);
          }
        });

      this.searchForm = this.fb.group(controls);

      this.showForm = true;
    } catch (error) {
      this.router.navigateByUrl('browse/anime');
      this.uiService.openDialog('Error', 'Cannot set search data. Try again.');
    }
  }

  getYearValues() {
    const yearNow = new Date().getFullYear() + 1;
    const firstYear = 1940;
    const yearsOptions = [];
    for (let index = yearNow; index >= firstYear; index--) {
      yearsOptions.push(index);
    }
    return yearsOptions;
  }

  search(forTitle: string = '') {
    const pathParts = location.pathname.split('/');
    const queryVariables = this.uiService.setQuerySearchVariables();
    this.queryParameters = { ...queryVariables, ...this.queryParameters };

    const values = this.searchForm.value;
    const keys = Object.keys(values);

    keys.forEach((key: string) => {
      let value: string = values[key];
      if (key !== 'title') {
        value = this.convertToVariable(value);
      }
      const newValue = { [key]: value };
      this.queryParameters = { ...this.queryParameters, ...newValue };

      if (!this.queryParameters[key]) {
        delete this.queryParameters[key];
      }
    });

    if (
      this.queryParameters.search &&
      !this.queryParameters.search.trim().replace(/_/g, '')
    ) {
      values.search = '';
      return;
    }

    let options: any = {
      queryParams: this.queryParameters,
    };
    let path: any = [`search/anime/all`];

    if (pathParts.length > 3) {
      options = { ...options, relativeTo: this.route };
      path = [];
    }

    this.router.navigate(path, options);
    this.anilistService.changeResults(this.queryParameters);
  }

  checkIfDisabled(option: string) {
    if (typeof option !== 'string') {
      return false;
    }
    if (option.includes(' label')) {
      return true;
    }
    return false;
  }

  removeLabel(option: string) {
    if (typeof option === 'string') {
      return option.replace(' label', '');
    }
    return option;
  }

  convertToVariable(format: string) {
    if (format === 'TV Show') {
      return 'TV';
    }
    if (typeof format === 'string') {
      return format.toUpperCase().split(' ').join('_');
    }
    return format;
  }
}
