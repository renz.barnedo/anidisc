import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Variable } from 'src/app/models/variable';
import { AnilistService } from 'src/app/services/anilist.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.scss'],
})
export class ViewAllComponent implements OnInit {
  noImage: string = this.uiService.noImage;
  showForm: boolean = false;
  filterForm: FormGroup = this.fb.group({});
  private page = 1;
  private noMoreMedia: boolean = false;
  media: any[] = [];

  labels: any[] = [
    {
      text: 'Romaji Title',
      value: 'TITLE_ROMAJI',
    },
    {
      text: 'English Title',
      value: 'TITLE_ENGLISH',
    },
    {
      text: 'Popularity',
      value: 'POPULARITY',
    },
    {
      text: 'Average Score',
      value: 'SCORE',
    },
    {
      text: 'Trending',
      value: 'TRENDING',
    },
    {
      text: 'Favourites',
      value: 'FAVOURITES',
    },
    {
      text: 'Date Added',
      value: 'UPDATED_AT',
    },
    {
      text: 'Release Date',
      value: 'START_DATE',
    },
  ];

  orders: string[] = ['Descending', 'Ascending'];

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const position =
      (document.documentElement.scrollTop || document.body.scrollTop) +
      document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;
    if (position == max) {
      this.viewMoreAnime();
    }
  }

  constructor(
    private anilistService: AnilistService,
    private uiService: UiService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.anilistService.resultsChange.subscribe((value: any) => {
      const type = location.pathname.split('/');
      if (type.length !== 4) {
        return;
      }
      this.media = [];
      this.displayMediaList(this.anilistService.media);
    });
  }

  async ngOnInit() {
    this.setOrderFields();
    this.displayMediaList();
  }

  setOrderFields() {
    const sort: string = this.uiService.getSortValue();
    const order: string = this.uiService.getOrderValue();
    const controls: any = {
      sort: this.fb.control(sort),
      order: this.fb.control(order),
    };
    this.filterForm = this.fb.group(controls);
    this.showForm = true;
  }

  setQueryVariables() {
    const queryVariables = this.uiService.setQuerySearchVariables();
    queryVariables.page = this.page;
    return queryVariables;
  }

  async displayMediaList(variables: any = null) {
    try {
      this.addSkeletonCards();
      const queryVariables: Variable = variables || this.setQueryVariables();
      queryVariables.page = this.page;
      const response: any = await this.anilistService.getMediaList(
        queryVariables
      );

      if (!response.length) {
        this.removeSkeletonCards();
        this.noMoreMedia = true;
        return;
      }
      this.setMediaDisplay(response);
    } catch (error) {
      this.router.navigateByUrl('browse/anime');
      this.uiService.openDialog('Error', 'Cannot get all data. Try again.');
    }
  }

  async viewMoreAnime() {
    if (this.noMoreMedia) {
      return;
    }
    this.page++;
    this.displayMediaList();
  }

  async filter() {
    if (this.filterForm.status === 'INVALID') {
      // TODO: display error modal maybe
      return;
    }

    this.media = [];
    this.addSkeletonCards();

    this.page = 1;
    const queryVariables = this.setQueryVariables();
    let { sort, order } = this.filterForm.value;

    if (order === 'Descending') {
      sort += '_DESC';
    }

    queryVariables.sort = sort;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: queryVariables,
    });

    const response = await this.anilistService.getMediaList(queryVariables);
    this.setMediaDisplay(response);
  }

  addSkeletonCards() {
    for (let index = 0; index < 20; index++) {
      this.media.push({ loading: true });
    }
  }

  removeSkeletonCards() {
    this.media = this.media.filter((card: any) => !card.loading);
  }

  setMediaDisplay(response: any) {
    const newMedia = response.map((media: any) => {
      media.description = media.description
        ? media.description
            .replace(/<br>/gi, '')
            .split('(')
            .filter(
              (string: string) => !string.toLowerCase().includes('source')
            )
        : '';
      const score = media.averageScore;
      media.icon = {
        color: score > 74 ? '#7BD555' : score > 60 ? '#F79A63' : '#E85D75',
        icon:
          score > 74 ? 'mood' : score > 60 ? 'sentiment_neutral' : 'mood_bad',
      };
      let color = media.coverImage.color;
      media.coverImage.color = color || '#2E3640';
      return media;
    });
    this.removeSkeletonCards();
    this.media = [...this.media, ...newMedia];
  }

  onCardClick(id: number, title: string) {
    this.uiService.onCardClick(id, title);
  }

  hexToRgb(hex: string) {
    const result: any = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    const red = parseInt(result[1], 16) - 250;
    const green = parseInt(result[2], 16) - 200;
    const blue = parseInt(result[3], 16) - 150;
    const rgbString = `rgba(${red}, ${green}, ${blue}, 0.7)`;
    return result ? rgbString : null;
  }

  lightOrDark(color: string) {
    return this.uiService.lightOrDark(color);
  }

  secondsToDhms(seconds: number) {
    return this.uiService.secondsToDhms(seconds);
  }
}
