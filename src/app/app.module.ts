import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './themes/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarComponent } from './utilities/toolbar/toolbar.component';
import { SearchComponent } from './utilities/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResultsComponent } from './utilities/results/results.component';
import { ViewAllComponent } from './pages/view-all/view-all.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { FullDetailsComponent } from './pages/full-details/full-details.component';
import { DialogComponent } from './utilities/dialog/dialog.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    SearchComponent,
    ResultsComponent,
    ViewAllComponent,
    FullDetailsComponent,
    DialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
