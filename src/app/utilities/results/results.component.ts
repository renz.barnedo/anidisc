import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnilistService } from 'src/app/services/anilist.service';
import { UiService } from 'src/app/services/ui.service';
import { Query } from '../../models/query';
import { Variable } from '../../models/variable';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit {
  media: any;
  width = window.screen.width;
  QUERY_VARIABLES: Query[] = [
    {
      label: 'TRENDING NOW',
      variables: this.setSortObject('TRENDING_DESC'),
    },
    {
      label: 'POPULAR THIS SEASON',
      variables: this.setPopularSeason('NOW'),
    },
    {
      label: 'UPCOMING NEXT SEASON',
      variables: this.setPopularSeason('NEXT'),
    },
    {
      label: 'ALL-TIME POPULAR',
      variables: this.setSortObject('POPULARITY_DESC'),
    },
    {
      label: 'FAVOURITES',
      variables: this.setSortObject('FAVOURITES_DESC'),
    },
    {
      label: 'TOP RATED ANIME',
      variables: this.setSortObject('SCORE_DESC'),
    },
  ];

  constructor(
    private anilistService: AnilistService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
    this.route.params.subscribe((value: any) => {
      this.getMedia();
    });
  }

  ngOnInit(): void {}

  setSortObject(value: string): Variable {
    return {
      sort: [value],
    };
  }

  setPopularSeason(seasonHint: string): Variable {
    // ? WINTER: Dec-Feb, SPRING: Mar-May, SUMMER: Jun-Aug, FALL: Sep-Nov
    // ? if month now = march to may, NOW = WINTER, NEXT = SPRING

    const date: Date = new Date();
    const nextSeasonYear: number = date.getFullYear();
    let currentSeasonYear: number = nextSeasonYear;
    const currentMonthIndex: number = date.getMonth();

    const seasons: string[] = [
      'WINTER',
      'WINTER',
      'SPRING',
      'SPRING',
      'SPRING',
      'SUMMER',
      'SUMMER',
      'SUMMER',
      'SUMMER',
      'FALL',
      'FALL',
      'FALL',
      'WINTER',
    ];

    const nextSeasonIndex: number = currentMonthIndex;
    let currentSeasonIndex: number = nextSeasonIndex - 1;
    if (currentSeasonIndex < 0) {
      currentSeasonIndex = 11;
      currentSeasonYear--;
    }

    const currentSeason: string = seasons[currentSeasonIndex];
    const nextSeason: string = seasons[nextSeasonIndex];
    let season: string = nextSeason;
    let seasonYear: number = nextSeasonYear;

    if (seasonHint === 'NOW') {
      season = currentSeason;
      seasonYear = currentSeasonYear;
    }

    const popularSeasonVariables: Variable = {
      season,
      seasonYear,
      sort: ['POPULARITY_DESC'],
    };

    return popularSeasonVariables;
  }

  async getMedia() {
    try {
      const queryVariables = this.QUERY_VARIABLES;
      this.addSkeletonCards(queryVariables);
      this.media = await this.anilistService.getMedia(queryVariables);
    } catch (error) {
      this.router.navigateByUrl('browse/anime');
      this.uiService.openDialog('Error', 'Cannot get results data. Try again.');
    }
  }

  addSkeletonCards(queryVariables: Query[]) {
    this.media = queryVariables.map((query: any) => {
      query.data = [];
      for (let index = 0; index < 6; index++) {
        query.data.push({ loading: true });
      }
      return query;
    });
  }

  removeSkeletonCards() {
    this.media = this.media.filter((card: any) => !card.loading);
  }

  viewAllMediaList(label: string) {
    const query: any = this.QUERY_VARIABLES.find(
      (variable: Query) => variable.label === label
    );
    const queryVariables: Variable = query.variables;
    queryVariables.perPage = 20;

    let path: string = this.location.path().replace('browse', 'search');
    path = `${path}/all`;

    this.router.navigate([path], {
      queryParams: queryVariables,
    });
  }

  onCardClick(id: number, title: string) {
    this.uiService.onCardClick(id, title);
  }

  showToolTip(title: any) {
    const {
      season,
      seasonYear,
      nextAiringEpisode,
      averageScore,
      studios,
      episodes,
      format,
      genres,
      coverImage,
    } = title;
    const data: any = {
      season,
      seasonYear,
      nextAiringEpisode,
      averageScore,
      studios,
      episodes,
      format,
      genres,
      color: coverImage && coverImage.color ? coverImage.color : '#ffffff',
    };
    // TODO: pass data to tooltip component
  }

  lightOrDark(color: string) {
    return this.uiService.lightOrDark(color);
  }
}
