export interface Dropdown {
  label: string;
  formControlName: string;
  options: string[];
}
