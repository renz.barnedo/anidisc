import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AnilistService } from 'src/app/services/anilist.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-full-details',
  templateUrl: './full-details.component.html',
  styleUrls: ['./full-details.component.scss'],
})
export class FullDetailsComponent implements OnInit {
  noImage: string = this.uiService.noImage;
  media: any;
  stats: any = {};
  showDetails: boolean = false;
  showSpoilerTags: boolean = false;
  spoilerTags: any[] = [];
  noSpoilerTags: any[] = [];
  displayedTags: any[] = [];
  relations: any[] = [];

  months: string[] = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];

  constructor(
    private anilistService: AnilistService,
    private uiService: UiService,
    private sanitizer: DomSanitizer,
    private router: Router
  ) {}

  ngOnInit(): void {
    try {
      this.getMediaData();
    } catch (error) {
      this.router.navigateByUrl('browse/anime');
      this.uiService.openDialog('Error', 'Cannot get media data. Try again.');
    }
  }

  async getMediaData() {
    const pathParts = location.pathname.split('/');
    const id: number = +pathParts[2];

    const response: any = await this.anilistService.getMediaFullDetails(id);
    this.media = response.data.details;

    this.media.characters.edges = this.media.characters.edges.map(
      (character: any) => {
        character.actor = character.voiceActors.find(
          (voiceActor: any) => voiceActor.language === 'JAPANESE'
        );
        return character;
      }
    );

    this.media.tags = this.media.tags.map((tag: any) => {
      tag.color = tag.isMediaSpoiler ? '#E15D75' : '#e5e7ea';
      return tag;
    });

    this.media.tags.forEach((tag: any) => {
      tag.isMediaSpoiler
        ? this.spoilerTags.push(tag)
        : this.noSpoilerTags.push(tag);
    });
    this.displayedTags = this.noSpoilerTags;

    this.relations = this.media.relations.edges;

    this.setStats(this.media);
    this.showDetails = true;
  }

  setStats(data: any) {
    let {
      airing,
      format,
      episodes,
      duration,
      status,
      startDate,
      endDate,
      season,
      seasonYear,
      averageScore,
      meanScore,
      popularity,
      favourites,
      studios,
      source,
      hashtag,
      genres,
      title,
      synonyms,
    } = data;

    if (airing) {
      const episode = airing.episode;
      const timeUntilAiring = this.secondsToDhms(airing.timeUntilAiring);
      this.stats.airing = `Ep ${episode}: ${timeUntilAiring}`;
    }
    this.stats.format = format;
    this.stats.episodes = episodes;
    this.stats.episodeDuration = duration + ' mins';
    this.stats.status = this.toTitleCase(status);
    const startMonthIndex = startDate.month - 1;
    const endMonthIndex = endDate.month - 1;
    this.stats.startDate = `${this.months[startMonthIndex]} ${startDate.day}, ${startDate.year}`;
    this.stats.endDate = `${this.months[endMonthIndex]} ${endDate.day}, ${endDate.year}`;
    this.stats.season = `${this.toTitleCase(season)} ${seasonYear}`;
    this.stats.averageScore = averageScore + '%';
    this.stats.meanScore = meanScore + '%';
    this.stats.popularity = popularity;
    this.stats.favorites = favourites;
    if (studios.nodes.length) {
      this.stats.studios = studios.nodes[0].name;
      this.stats.producers = studios.nodes
        .map((node: any, index: number) => {
          if (index > 0) {
            return node.name;
          }
        })
        .filter((node: string) => node);
    }

    this.stats.source = source;
    this.stats.hashtag = hashtag;
    this.stats.genres = genres.join(', ');
    this.stats.romaji = title.romaji;
    this.stats.english = title.english;
    this.stats.native = title.native;
    this.stats.synonyms = synonyms;
    this.getStatsKeys();
  }

  secondsToDhms(seconds: number) {
    return this.uiService.secondsToDhms(seconds);
  }

  toggleSpoilerTagsDisplay() {
    this.showSpoilerTags = !this.showSpoilerTags;
    this.displayedTags = this.showSpoilerTags
      ? this.media.tags
      : this.noSpoilerTags;
  }

  getTrailerSource(trailer: any) {
    if (trailer && trailer.site === 'youtube') {
      let url: any = `https://www.youtube.com/embed/${trailer.id}`;
      url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
      return url;
    }
    return '';
  }

  toggleRelationsDetails(index: number) {
    if (this.relations.length < 4) {
      return;
    }
    this.relations[index].hovered = !this.relations[index].hovered;
  }

  filterNotSpoilerTags() {
    return this.media.tags.filter((tag: any) => !tag.isMediaSpoiler);
  }

  camelToTitleCase(string: string) {
    string = this.uiService.camelToTitleCase(string);
    return string;
  }

  toTitleCase(string: string) {
    return this.uiService.toTitleCase(string, ' ');
  }

  underscoretoTitleCase(string: string) {
    return string === 'TV' ? 'TV' : this.uiService.toTitleCase(string, '_');
  }

  getStatsKeys() {
    return Object.keys(this.stats);
  }

  isSingleString(key: string) {
    return key !== 'synonyms' && key !== 'producers';
  }

  nullChecker(value: any) {
    if (typeof value === 'string') {
      return value && !value.includes('null');
    }
    return value;
  }
}
