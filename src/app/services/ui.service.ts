import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Variable } from '../models/variable';
import { DialogComponent } from '../utilities/dialog/dialog.component';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  noImage: string =
    'https://t4.ftcdn.net/jpg/02/07/87/79/360_F_207877921_BtG6ZKAVvtLyc5GWpBNEIlIxsffTtWkv.jpg';

  constructor(
    private location: Location,
    private router: Router,
    private dialog: MatDialog
  ) {}

  onCardClick(id: number, title: string) {
    const pathname = this.location.path();
    const type = pathname.toLowerCase().includes('anime') ? 'anime' : 'manga';
    const url = `${type}/${id}/${title
      .replace(/[0-9]|(|)/g, '')
      .replace(/ /g, '-')}`;
    this.router.navigateByUrl(url);
  }

  openDialog(title: string, content: string) {
    this.dialog.open(DialogComponent, {
      data: { title, content },
      panelClass: 'dialog-template',
    });
  }

  getQuerySearch(): string[] {
    return location.search.replace('?', '').split('&');
  }

  getSortVariable() {
    return this.getQuerySearch().find((parameter: string) =>
      parameter.includes('sort')
    );
  }

  getOrderValue(): string {
    const sortVariable: any = this.getSortVariable();
    if (!sortVariable) {
      return '';
    }
    const hasDesc: boolean = sortVariable.includes('DESC');
    const order: string = hasDesc || !sortVariable ? 'Descending' : 'Ascending';
    return order;
  }

  getSortValue(): string {
    const sortVariable: any = this.getSortVariable();
    if (!sortVariable) {
      return '';
    }
    const sort: any[] = sortVariable.split('=');
    const value: string = sort[1].replace('_DESC', '');
    return value;
  }

  setQuerySearchVariables(): Variable {
    const variables = this.getQuerySearch().reduce(
      (allVariables: Variable, query: string) => {
        const parts = query.split('=');
        const key = parts[0];
        const value = parts[1];
        const variable = { [key]: value };
        return { ...allVariables, ...variable };
      },
      {}
    );
    return variables;
  }

  toTitleCase(string: string, splitter: string) {
    if (typeof string !== 'string') {
      return string;
    }
    if (!string || !splitter) {
      return '';
    }
    let words = string.split(splitter);
    words = words.map(
      (word: string) => word[0].toUpperCase() + word.substring(1).toLowerCase()
    );
    string = words.join(' ');
    return string;
  }

  camelToTitleCase(string: string) {
    if (!string) {
      return '';
    }
    let words: any = string.split(/(?=[A-Z])/);
    words = words.join(' ');
    string = this.toTitleCase(words, ' ');
    return string;
  }

  lightOrDark(color: any) {
    if (!color) {
      return '#000000';
    }
    let red: any;
    let green: any;
    let blue: any;

    if (color.match(/^rgb/)) {
      color = color.match(
        /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
      );

      red = color[1];
      green = color[2];
      blue = color[3];
    } else {
      color = +(
        '0x' + color.slice(1).replace(color.length < 5 && /./g, '$&$&')
      );

      red = color >> 16;
      green = (color >> 8) & 255;
      blue = color & 255;
    }

    const hsp = Math.sqrt(
      0.299 * (red * red) + 0.587 * (green * green) + 0.114 * (blue * blue)
    );

    if (hsp > 150) {
      return '#000000';
    } else {
      return '#ffffff';
    }
  }

  secondsToDhms(seconds: number) {
    seconds = Number(seconds);
    const day = Math.floor(seconds / (3600 * 24));
    const hour = Math.floor((seconds % (3600 * 24)) / 3600);
    const minute = Math.floor((seconds % 3600) / 60);

    const dayDisplay = day > 0 ? day + (day == 1 ? ' day, ' : ' days, ') : '';
    const hourDisplay = hour > 0 ? hour + (hour == 1 ? ' hour' : ' hours') : '';
    const minuteDisplay =
      minute > 0 ? minute + (minute == 1 ? ' minute ' : ' minutes ') : '';
    return dayDisplay
      ? dayDisplay + hourDisplay
      : `${hourDisplay}, ${minuteDisplay}`;
  }
}
