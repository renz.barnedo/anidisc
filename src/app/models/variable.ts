export interface Variable {
  sort?: string[];
  seasonYear?: number;
  season?: string;
  perPage?: number;
  type?: string;
  page?: number;
  format?: string;
  genre?: string;
  search?: string;
  source?: string;
  status?: string;
}
