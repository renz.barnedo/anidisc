import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  private lastScrollTop: number = 0;
  private solid: string = '#152232';
  private transparent: string = 'rgba(35, 38, 67, 0.5)';
  toolbarBackground: string = this.solid;
  hideToolbar: boolean = false;
  isInFullDetails: boolean = false;
  private deferredPrompt: any;

  constructor(private router: Router, private uiService: UiService) {
    this.setToolbarBackground(location.pathname);
    this.router.events.subscribe((val: any) => {
      if (val instanceof NavigationEnd) {
        this.setToolbarBackground(val.url);
      }
    });
  }

  ngOnInit(): void {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
    });
    window.addEventListener('appinstalled', (evt) => {
      this.uiService.openDialog('Success', 'Check your apps list.');
    });
    window.addEventListener('DOMContentLoaded', () => {
      let displayMode = 'browser tab';
      const navigatorApi: any = navigator;
      if (navigatorApi.standalone) {
        displayMode = 'standalone-ios';
      }
      if (window.matchMedia('(display-mode: standalone)').matches) {
        displayMode = 'standalone';
      }
      if (displayMode.indexOf('standalone') > -1) {
        localStorage.hasInstalledPwa = true;
        this.uiService.openDialog('Success', 'Check your apps list.');
      }
    });
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent() {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    this.hideToolbar = scrollTop > this.lastScrollTop;
    const stillScrollingUp = !this.hideToolbar && scrollTop;
    this.toolbarBackground = stillScrollingUp
      ? this.solid
      : this.isInFullDetails
      ? this.transparent
      : this.solid;
    this.lastScrollTop = scrollTop <= 0 ? 0 : scrollTop;
  }

  setToolbarBackground(pathname: string) {
    const subdirectories = pathname.split('/');
    this.isInFullDetails = subdirectories[1] === 'anime';
    this.toolbarBackground = this.isInFullDetails
      ? this.transparent
      : this.solid;
  }

  installAsPwa() {
    if (!this.deferredPrompt) {
      this.uiService.openDialog('Success', 'Check your apps list.');
      return;
    }
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then((choiceResult: any) => {
      if (choiceResult.outcome === 'accepted') {
        this.uiService.openDialog('Success', 'Check your apps list.');
      } else {
        this.uiService.openDialog('Error', 'App install failed.');
      }
    });
  }
}
