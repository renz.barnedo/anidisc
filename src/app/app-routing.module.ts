import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullDetailsComponent } from './pages/full-details/full-details.component';
import { HomeComponent } from './pages/home/home.component';
import { ViewAllComponent } from './pages/view-all/view-all.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'browse/anime',
    pathMatch: 'full',
  },
  {
    path: 'browse/:type',
    component: HomeComponent,
  },
  {
    path: 'search/:type/:category',
    component: ViewAllComponent,
  },
  {
    path: ':type/:id/:title',
    component: FullDetailsComponent,
  },
  { path: '**', redirectTo: '/browse/anime' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
